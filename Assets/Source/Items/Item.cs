﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

//Intended to be used as a base class for other item types
//such as weapons, medkit, ect.
public class Item : MonoBehaviour
{
    public bool IsHeld { get { return isHeld; } }
    //Made TeamId a common item attribute so that player controller
    //only needs to know and look for the Item class
    public Team TeamId { get { return team; } }
    private Rigidbody2D rb;
    private bool isHeld = false;
    private Team team = Team.Unset;

    private Weapon weapon;

    // Use this for initialization
    virtual protected void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    //Empty method included so it can be overridden
    virtual protected void Update() { }

    virtual public void Use() { }

    virtual public void OnPickup(Transform anchorPoint, Team team)
    {
        rb.simulated = false;
        isHeld = true;
        //rotating before parenting because it gets weird if it
        //rotates after parenting
        this.transform.localRotation = Quaternion.identity;
        this.transform.SetParent(anchorPoint);
        this.transform.localPosition = Vector3.zero;

    }

    virtual public void OnDrop()
    {
        //BUG: when physics simulation is resumed on item, it pushes 
        //the player a bit, currently results in both the item and 
        //player moving (the item moving away is desireable, just not
        //the player moving)
        this.transform.SetParent(null);
        rb.simulated = true;
        isHeld = false;
        team = Team.Unset;
    }

}
