﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item
{
    public float cooldown = 0.5F;
    public Transform ProjectileLaunchTransform;
    private float timer = 0F;
    private bool fired;
    //todo: clean out dependency on player controller
    private PlayerController playerController;

    override protected void Update()
    {
        base.Update();
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        if (fired && timer <= 0)
        {
            fired = false;
            timer = cooldown;
            FireBullet();
        }
    }

    override public void Use()
    {
        base.Use();
        this.Fire();
    }

    private void FireBullet()
    {
        if (playerController != null)
        {
            GameObject bulletObj = PoolingManager.current.GetObject();
            BulletScript bullet = bulletObj.GetComponent<BulletScript>();
            if (bulletObj == null) return;
            if (bullet == null)
            {
                Debug.LogError("Bullet is missing BulletScript component");
                return;
            }

            bulletObj.transform.position = this.ProjectileLaunchTransform.position;

            bullet.OwnerTeam = this.TeamId;
            bullet.Direction = this.playerController.facingRight ? Vector2.right : Vector2.left;

            bulletObj.SetActive(true);
        }
        else
        {
            Debug.LogWarning("Gun tried to fire without a player attached to it, plz fix");
        }
    }

    override public void OnPickup(Transform anchorPoint, Team teamId)
    {
        base.OnPickup(anchorPoint, teamId);
        this.playerController = anchorPoint.parent.GetComponent<PlayerController>();
        if (playerController == null)
        {
            Debug.LogError("Weapon did not find component 'PlayerController' on parent of 'anchorPoint'");
        }
    }

    override public void OnDrop()
    {
        base.OnDrop();
        this.playerController = null;
    }

    public void Fire()
    {
        fired = true;
    }
}
