﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletDisable))]
[RequireComponent(typeof(Collider2D))]
public class BulletScript : MonoBehaviour
{
    public Team OwnerTeam;
    public Vector2 Direction;
    // Bullet characteristics
    [SerializeField]
    private float speed;
    [SerializeField]

    void Update()
    {
        transform.Translate(((Vector3)Direction) * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //don't collide with items (layer 8 is items)
        if (other.gameObject.layer == 8)
            return;

        //don't collide with other projectiles
        if (other.gameObject.CompareTag("Projectile"))
            return;

        // collide with damagable objects, but only if it is not the one who fired the bullet
        // then kill that player
        var otherHealth = other.gameObject.GetComponent<Health>();
        if (otherHealth != null)
        {
            var teamId = other.gameObject.GetComponent<TeamIdentity>();
            if (teamId.Id != OwnerTeam)
                otherHealth.TakeDamage(100);
            else
                return; // we are collider with the owner, we want to pass through
        }

        //collide with everything else
        gameObject.SetActive(false);
    }
}
