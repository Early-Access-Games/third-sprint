﻿using UnityEngine;
using System.Collections;

public class FrameStepper : MonoBehaviour {
    public bool IsOn;
    bool IsStepping;

    public void Toggle() {
        IsOn = !IsOn;
    }

    public void Step() {
        IsStepping = true;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.O)) {
            Toggle();
        }
        else if (Input.GetKeyDown(KeyCode.P)) {
            Step();
        }

        if (IsStepping) {
            Time.timeScale = 1;
            IsStepping = false;
        }
        else {
            Time.timeScale = (IsOn ? 0 : 1);
        }
    }
}