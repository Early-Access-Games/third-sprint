﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnchorScript : MonoBehaviour {

	public float areaSize;
	private float defaultCameraSize = 6.0f;

	void Start(){
		//areaSize = 10f;
		//areaSize = newSize;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Player") {
			CameraScript.current.target = this.gameObject;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Player") {
			CameraScript.current.target = GameObject.Find ("Player");
		}
	}
}
