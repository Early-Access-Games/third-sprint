﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingManager : MonoBehaviour
{

    /* Script by Harrold Paderan 28-October-2017 				*
	 * Derived from Object Pooling tutorial by Mike Geig, Unity	*/

    public static PoolingManager current;
    [SerializeField]
    private GameObject objectType;
    [SerializeField]
    private int pooledAmount;
    [SerializeField]
    private bool willGrow = true;

    List<GameObject> objectList;

    void Awake()
    {
        if (current != null)
            Debug.LogWarning("FYI: A PollingManager has already been initalized and assigned to 'PoolingManager.current', but another PollingManger is being initalized and is overwritting 'PoolingManger.current'. This is likely undesired behaviour.");

        current = this;
    }

    void Start()
    {   // Initialize pool
        objectList = new List<GameObject>();
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(objectType);
            obj.SetActive(false);
            objectList.Add(obj);
        }
    }

    public GameObject GetObject()
    {   //search for inactive element in pool and return
        for (int i = 0; i < objectList.Count; i++)
        {
            if (!objectList[i].activeInHierarchy)
            {
                return objectList[i];
            }
        }

        if (willGrow)
        {   // add elements to pool if all elements are in use
            GameObject obj = (GameObject)Instantiate(objectType);
            objectList.Add(obj);
            return obj;
        }

        // if pool is not set to grow, return null if all elements are in use
        Debug.LogWarning(String.Format("A pooled object was requested, but the object pool for {0} has run out", this.name));
        return null;
    }

}
