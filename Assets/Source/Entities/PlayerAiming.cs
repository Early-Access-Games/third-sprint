﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAiming : MonoBehaviour {
    [Header("Debug only")]
    public Vector2 direction;
    public bool hasItem = false;
    public bool fire = false;
    //variable for weapon class
    //rename to PlayerUse;;; this is probably redundant
    private PlayerController playerController;

    private void Awake() {
        playerController = GetComponent<PlayerController>();
    }

    private void Update() {
        if (playerController.facingRight && hasItem && fire) {
            fire = false;
            //fire weapon object
        }
    }

    //add variable to pass in weapon object
    public void ToggleItem() {
        hasItem = !hasItem;
        //set weapon object
    }

    public void FireWeapon() {
        fire = true;
    }
}
