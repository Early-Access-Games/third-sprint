﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public enum PlayerPowerEnum
{
    MeleeAttack,
    WallClimbing,
    Gliding,
    Power4,
    Power5,
    Power6
}

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(TeamIdentity))]
public class PlayerController : MonoBehaviour
{
    public enum MoveState
    {
        Normal,
        WallGripped,
        WallJumping,
        WallJumped,
        Swimming
    }

    public enum JumpState
    {
        Airborn,
        AirbornNoForgive,
        OnWall,
        OnGround,
        SwimmingInWater
    }

    public enum JumpAction
    {
        None,
        Jump,
        WallJump
    }

    [Header("Player")]
    public string playerVerticalMove = "Vertical_P1";
    public string playerHorizontalMove = "Horizontal_P1";
    public string playerJump = "Jump_P1";
    public string playerPause = "Pause_P1";
    public string playerFire = "Fire1_P1";
    public string playerPickup = "Pickup_P1";
    private Health health;
    private TeamIdentity teamId;

    [Header("Movement")]
    public MoveState currentMoveState = MoveState.Normal;
    public int maxSpeed = 15;
    public bool facingRight { get; private set; }
    public float wallJumpControlLossDuration = 0.05f;
    private float movementInputHorizontal;
    private float movementInputVertical;
    public bool inWater = false;

    [Header("Jumping")]
    public JumpState currentJumpState = JumpState.Airborn;
    public JumpAction currentJumpAction = JumpAction.None;
    public int jumpForce = 50;
    public float groundThreshold = 0.025F;
    public float wallThreshold = 0.025F;
    public LayerMask whatIsGround;
    private RaycastHit2D[] groundHits = new RaycastHit2D[1];
    public LayerMask whatIsWall;
    public float forgivableAirTime = 1f;
    private float airTime = 0f;
    private RaycastHit2D[] wallHits = new RaycastHit2D[1];
    private float playerHead;
    private float playerFeet;
    private float playerRight;
    private float playerLeft;

    [Header("Wall Mechanics")]
    public float wallSlideGravity = 3F;
    public float wallGripTime = 0.1f;
    private float normalGravity;
    private float wallGripTimeRemaining;

    [Header("Item Interaction")]
    public Transform itemAnchorPoint;
    public float pickupRange = 1.0f;
    private Item currentItem = null;
    private bool pickupKeyHit = false;

    [Header("Player Powers")]
    public string playerToggleNextPower = "ToggleNextPower_P1";
    public string playerTogglePreviousPower = "TogglePreviousPower_P1";
    public string playerUseCurrentPower = "UseCurrentPower_P1";
    public PlayerPowerEnum currentPlayerPower;
    public int MeleeAttackDamage = 10;
    public float MeleeAttackForce = 60f;
    public Vector2 MeleeAttackOriginOffset = new Vector2(5f, -0.24f);
    public Vector2 MeleeAttackHitBoxSize = new Vector2(10.96f, 5.05f);
    private int powerCount = 6;
    public float glidingEffectiveness = 10F;

    [Header("Debug")]
    public bool debug = false;
    private float buffer = 0.01F; //used for calculations; don't touch

    private Rigidbody2D rigidBody;
    private Animator animator;
    private GameManager game;
    private BoxCollider2D boxCollider;
    private float spriteHeight;
    private float spriteWidth;

    // Use this for initialization
    private void Awake()
    {
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        rigidBody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();


        //fallback if anchor point is not set
        if (itemAnchorPoint == null)
            itemAnchorPoint = this.transform.Find("ItemAnchorPoint");

        powerCount = Enum.GetNames(typeof(PlayerPowerEnum)).Length;

        //setup health
        health = this.GetComponent<Health>();
        health.OnAlive = this.OnAlive;
        health.OnDeath = this.OnDeath;

        //setup team id
        teamId = this.GetComponent<TeamIdentity>();
    }

    private void Start()
    {
        normalGravity = rigidBody.gravityScale;
        facingRight = true;
    }

    private void Update()
    {
        if (debug)
            Debugging();

        //generic controls
        if (Input.GetButtonDown(playerPause))
            game.TogglePause();
        //use
        if (Input.GetButtonDown(playerFire) && currentItem != null)
            currentItem.Use();

        //item interaction
        if (Input.GetButtonDown(playerPickup))
            PickupItem();

        //movement input polling
        movementInputHorizontal = Input.GetAxis(playerHorizontalMove);
        movementInputVertical = Input.GetAxis(playerVerticalMove);

        //update direction character is facing based on movement
        if (movementInputHorizontal > 0 && !facingRight && currentMoveState != MoveState.WallGripped)
            Flip();
        else if (movementInputHorizontal < 0 && facingRight && currentMoveState != MoveState.WallGripped)
            Flip();

        //toggle through player powers with - and =
        if (Input.GetButtonDown(playerToggleNextPower))
        {
            currentPlayerPower += 1;
            if ((int)currentPlayerPower >= powerCount)
                currentPlayerPower = 0;
        }

        if (Input.GetButtonDown(playerTogglePreviousPower))
        {
            currentPlayerPower -= 1;
            if ((int)currentPlayerPower < 0)
                currentPlayerPower = (PlayerPowerEnum)(powerCount - 1);
        }

        if (Input.GetButtonDown(playerUseCurrentPower))
        {
            UsePower();
        }
        if (currentPlayerPower == PlayerPowerEnum.Gliding)
        {
            if (rigidBody.velocity.y < 0)
            {
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, rigidBody.velocity.y / glidingEffectiveness);
            }
        }

        UpdateBoundaryCalculations();
        UpdateJumpState();

        //animation
        animator.SetBool("Right", facingRight);
        animator.SetFloat("Speed", Mathf.Abs(rigidBody.velocity.x));
        animator.SetFloat("vSpeed", Mathf.Abs(rigidBody.velocity.y)); //change mathabs later when anims are set in
        animator.SetBool("Grounded", IsGrounded());
        animator.SetBool("Walled", IsOnWalled());
    }

    float timeTillControlRegained;

    private void FixedUpdate()
    {
        switch (currentMoveState)
        {
            case (MoveState.WallGripped):
                //wall climbing now allowed when gripped to the wall
                if (currentPlayerPower == PlayerPowerEnum.WallClimbing)
                {
                    if (movementInputHorizontal != 0)
                    {
                        rigidBody.velocity = new Vector2(movementInputHorizontal * maxSpeed, rigidBody.velocity.y);
                    }

                    if (movementInputVertical != 0)
                    {
                        rigidBody.velocity = new Vector2(rigidBody.velocity.x, movementInputVertical * maxSpeed);
                    }
                }
                break;
            case (MoveState.Normal):
                //not adjusting velocity when there's no input results in sliding around
                if (movementInputHorizontal != 0)
                {
                    rigidBody.velocity = new Vector2(movementInputHorizontal * maxSpeed, rigidBody.velocity.y);
                }
                break;
            case (MoveState.WallJumping):
                currentMoveState = MoveState.WallJumped;
                timeTillControlRegained = wallJumpControlLossDuration;
                break;
            case (MoveState.WallJumped):
                timeTillControlRegained -= Time.deltaTime;

                if (timeTillControlRegained <= 0f)
                {
                    currentMoveState = MoveState.Normal;
                }
                break;
            case (MoveState.Swimming):
                rigidBody.velocity = new Vector2(movementInputHorizontal * maxSpeed, movementInputVertical * maxSpeed);
                break;
        }

        switch (currentJumpAction)
        {
            case (JumpAction.Jump):
                Jump();
                break;
            case (JumpAction.WallJump):
                WallJump();
                break;
        }

        currentJumpAction = JumpAction.None;
    }

    void OnDeath()
    {
        Debug.Log("DED");
        this.enabled = false;
        this.ChangeColor(Color.red);
    }

    void OnAlive()
    {
        Debug.Log("NOT DED");
        this.enabled = true;
        this.ChangeColor(Color.white);
    }

    void OnGUI()
    {
        if (this.debug)
        {
            GUI.Label(new Rect(10, 10, 400, 20), "PLAYER DEBUG - DO NOT SHIP");
            GUI.Label(new Rect(30, 30, 400, 20), "Power: " + currentPlayerPower.ToString());
            GUI.Label(new Rect(30, 50, 400, 20), String.Format("Health: {0}/100", this.health.HealthValue)); //todo, update when after rewriting the health component
        }
    }

    private void UpdateBoundaryCalculations()
    {
        playerFeet = CalculateBottom();
        playerHead = CalculateTop();
        playerRight = CalculateRight();
        playerLeft = CalculateLeft();
    }

    private void UpdateJumpState()
    {
        switch (currentJumpState)
        {
            case (JumpState.Airborn):
                AirbornState();
                break;
            case (JumpState.AirbornNoForgive):
                AirbornNoForgiveState();
                break;
            case (JumpState.OnGround):
                OnGroundState();
                break;
            case (JumpState.OnWall):
                OnWallState();
                break;
            case (JumpState.SwimmingInWater):
                OnSwimState();
                break;
        }
    }

    private void AirbornState()
    {
        if (inWater)
        {
            currentJumpState = JumpState.SwimmingInWater;
            currentMoveState = MoveState.Swimming;
            return;
        }
        if (IsGrounded())
        {
            currentJumpState = JumpState.OnGround;
            airTime = 0;

            //this frame player is actually on the ground
            //so let em jump
            if (Input.GetButtonDown(playerJump))
            {
                currentJumpAction = JumpAction.Jump;
                currentJumpState = JumpState.AirbornNoForgive;
            }
            return;
        }

        if (IsOnWalled())
        {
            currentJumpState = JumpState.OnWall;
            airTime = 0;

            //this frame player is actually on the wall,
            //so let em jump
            if (Input.GetButtonDown(playerJump))
            {
                currentJumpAction = JumpAction.WallJump;
                currentJumpState = JumpState.AirbornNoForgive;
            }
            return;
        }

        if (Input.GetButtonDown(playerJump))
        {
            currentJumpAction = JumpAction.Jump;
            currentJumpState = JumpState.AirbornNoForgive;
            airTime = 0;
            return;
        }

        if (airTime > forgivableAirTime)
        {
            currentJumpState = JumpState.AirbornNoForgive;
            airTime = 0;
            return;
        }

        airTime += Time.deltaTime;
    }

    private void AirbornNoForgiveState()
    {
        if (inWater)
        {
            currentJumpState = JumpState.SwimmingInWater;
            currentMoveState = MoveState.Swimming;
            return;
        }
        if (IsOnWalled())
        {
            currentJumpState = JumpState.OnWall;

            if (Input.GetButtonDown(playerJump))
            {
                currentJumpAction = JumpAction.WallJump;
                currentJumpState = JumpState.AirbornNoForgive;
            }
            return;
        }
        if (IsGrounded() && currentJumpAction != JumpAction.Jump)
        {
            currentJumpState = JumpState.OnGround;
            OnGroundState();
            return;
        }
    }

    private void OnGroundState()
    {
        if (inWater)
        {
            currentJumpState = JumpState.SwimmingInWater;
            currentMoveState = MoveState.Swimming;
            return;
        }
        if (Input.GetButtonDown(playerJump))
        {
            currentJumpAction = JumpAction.Jump;
            currentJumpState = JumpState.AirbornNoForgive;
            return;
        }

        if (!IsGrounded())
        {
            currentJumpState = JumpState.Airborn;
            return;
        }
    }

    //wall grip complicated enough that it need its own substate lol
    enum WallGripState
    {
        PreGrip,
        Gripped,
        Sliding
    }
    WallGripState currentWallGripState;
    private void OnWallState()
    {
        if (inWater)
        {
            currentJumpState = JumpState.SwimmingInWater;
            currentMoveState = MoveState.Swimming;
            return;
        }
        if (!IsOnWalled())
        {
            currentJumpState = JumpState.AirbornNoForgive;
            rigidBody.gravityScale = normalGravity;
            currentWallGripState = WallGripState.PreGrip;
            currentMoveState = MoveState.Normal;
            return;
        }

        if (IsGrounded())
        {
            currentJumpState = JumpState.OnGround;
            rigidBody.gravityScale = normalGravity;
            currentWallGripState = WallGripState.PreGrip;
            currentMoveState = MoveState.Normal;
            return;
        }

        if (currentPlayerPower == PlayerPowerEnum.WallClimbing && Input.GetButtonDown(playerJump))
        {
            currentJumpAction = JumpAction.WallJump;
            currentJumpState = JumpState.AirbornNoForgive;
            currentMoveState = MoveState.WallJumping;
            rigidBody.gravityScale = normalGravity;
            currentWallGripState = WallGripState.PreGrip;
            return;
        }

        switch (currentWallGripState)
        {
            case (WallGripState.PreGrip):
                wallGripTimeRemaining = wallGripTime;
                //check if player is falling yet
                if (rigidBody.velocity.y <= 0f)
                {
                    currentWallGripState = WallGripState.Gripped;
                    currentMoveState = MoveState.WallGripped;
                }
                break;
            case (WallGripState.Gripped):
                rigidBody.gravityScale = 0f;
                //if wall climbing we just sit in this state, until the
                //player pushes off or jumps off the wall

                //we'll start counting down the grip time regardless of the wall climb power
                //so that the player starts sliding down as soon as they switch off of it
                wallGripTimeRemaining -= Time.deltaTime;

                if (currentPlayerPower != PlayerPowerEnum.WallClimbing)
                {
                    //count down time, then kick into next state
                    if (wallGripTimeRemaining < 0f)
                    {
                        currentWallGripState = WallGripState.Sliding;
                        currentMoveState = MoveState.Normal;
                    }
                }

                break;
            case (WallGripState.Sliding):
                //if wall climbing is active, we no longer go to this state
                rigidBody.gravityScale = wallSlideGravity;
                break;
        }
    }

    private void OnSwimState()
    {
        if (inWater)
        {
            currentJumpState = JumpState.SwimmingInWater;
            currentMoveState = MoveState.Swimming;
        }
        else
        {
            currentJumpState = JumpState.Airborn;
            currentMoveState = MoveState.Normal;
        }
    }

    private bool IsGrounded()
    {
        return Physics2D.LinecastNonAlloc(
            new Vector2(playerLeft, playerFeet), //left
            new Vector2(playerLeft, playerFeet - groundThreshold),
            groundHits, whatIsGround) +
            Physics2D.LinecastNonAlloc(
            new Vector2(rigidBody.position.x, playerFeet), //middle
            new Vector2(rigidBody.position.x, playerFeet - groundThreshold),
            groundHits, whatIsGround) +
            Physics2D.LinecastNonAlloc(
            new Vector2(playerRight, playerFeet), //right
            new Vector2(playerRight, playerFeet - groundThreshold),
            groundHits, whatIsGround) != 0;

    }

    private bool IsOnWalled()
    {
        //BLACK MAGIC MATH
        return Physics2D.LinecastNonAlloc(
            new Vector2(playerRight, rigidBody.position.y), //middle
            new Vector2(playerRight + wallThreshold, rigidBody.position.y),
            wallHits, whatIsWall) +
            Physics2D.LinecastNonAlloc(
            new Vector2(playerLeft, rigidBody.position.y), //middle
            new Vector2(playerLeft - wallThreshold, rigidBody.position.y),
            wallHits, whatIsWall) > 0;
        //is true if greater than zero for some reason.... ?????
    }

    //TEMP: remove this, and the calls to it from PlayerHealth once we have actual sprites, just for testing
    public void ChangeColor(Color color)
    {
        //this.GetComponent<SpriteRenderer>().color = color;
    }

    private void PickupItem()
    {
        Item i = null;

        var objectsHit = Physics2D.CircleCastAll(
            this.transform.position,
            pickupRange,
            Vector2.up,
            0f,
            LayerMask.GetMask("Items"));

        //item found if there's at least one object
        //hit by the circle cast
        bool itemFound = objectsHit.Length > 0;

        if (itemFound)
        {
            //this is initalized to make the compiler happy
            //not actually using this empty hit info
            RaycastHit2D closestHit = new RaycastHit2D();
            float shortestDistance = float.MaxValue;

            //grab the closest item
            foreach (RaycastHit2D hit in objectsHit)
            {
                var p = (Vector2)this.transform.position;
                var heading = hit.point - p;
                var distance = heading.magnitude;
                var direction = heading / distance;

                //check if there's anything in the way of picking up the item
                var lineOfSightCheck = Physics2D.Raycast(p, direction, distance, LayerMask.GetMask("Default"));

                if (lineOfSightCheck.collider == null && hit.distance <= shortestDistance)
                    closestHit = hit;
            }

            //is null if object is on the other side of a wall, or something else is in the way
            if (closestHit.transform != null)
                i = closestHit.transform.GetComponent<Item>();
        }

        if (i != null && !i.IsHeld)
        {
            {
                if (currentItem != null)
                {
                    currentItem.OnDrop();
                }

                currentItem = i;
                i.OnPickup(itemAnchorPoint, teamId.Id);
            }
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void Jump()
    {
        rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
        rigidBody.AddForce(this.transform.up * jumpForce, ForceMode2D.Impulse);
    }

    // as Jon pointed out: movement gets overwritten by the player's movement speed so if a direction keypress is taken during a wall jump
    // (for example in the opposite direction) it will not be as fast as a regular wall jump
    // This directly conflics the idea of having full air control on the player
    // other games that technically have both use acceleration on their player movement; we do not (we only have decceleration)
    private void WallJump()
    {
        if (facingRight)
            rigidBody.AddForce(new Vector2(-1F * (jumpForce / 1.25F), jumpForce), ForceMode2D.Impulse);
        else
            rigidBody.AddForce(new Vector2((jumpForce / 1.25F), jumpForce), ForceMode2D.Impulse);
        Flip();
    }

    private void UsePower()
    {
        switch (currentPlayerPower)
        {
            case PlayerPowerEnum.MeleeAttack:
                UseMeleeAttack();
                break;
            default:
                UnimplementedPower();
                break;
        }
    }

    private void UseMeleeAttack()
    {
        //Debug.Log("MELEE ATTKC");
        var p = this.transform.position;
        var results = new Collider2D[16];
        var filter = new ContactFilter2D();
        //hit everything but player
        filter.useLayerMask = true;
        filter.layerMask = ~LayerMask.GetMask("Player");

        Vector2 hitBoxOrigin;
        //xdir is used for which direction to apply the melee attack in (hitbox and force)
        float xDir = facingRight ? 1f : -1f;

        //adjust origin, offsetting it from the players position depending on the direction the player is facing
        hitBoxOrigin = new Vector2(p.x + (MeleeAttackOriginOffset.x * xDir), p.y + MeleeAttackOriginOffset.y);

        int c = Physics2D.OverlapBox(hitBoxOrigin, MeleeAttackHitBoxSize, 0f, filter, results);
        if (c > 0)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Melee attack hits:");
            for (int i = 0; i < c; i++)
            {
                sb.AppendLine(results[i].gameObject.name);
                var otherHealth = results[i].GetComponent<Health>();
                var otherRB = results[i].GetComponent<Rigidbody2D>();
                if (otherHealth != null)
                    otherHealth.TakeDamage(MeleeAttackDamage);
                if (otherRB != null)
                {
                    Vector2 force = new Vector2(xDir, 0f) * MeleeAttackForce;
                    otherRB.AddForce(force, ForceMode2D.Impulse);
                    Debug.Log(force);
                }

            }
            Debug.Log(sb.ToString());
        }
    }

    private void UnimplementedPower()
    {
        Debug.LogError("Unimplemented power called: " + currentPlayerPower.ToString());
    }

    // add/subtract from center of the transform
    // the scaled version of the collider's edges , offset must be 0 because of flipping scales (literally)
    // add/subtract a buffer so it's not on the literal edge of the collider, which causes problems w/ physics
    private float CalculateRight()
    {
        return this.transform.position.x + (Mathf.Abs(this.transform.localScale.x) * (boxCollider.size.x / 2F)) - buffer;
    }

    private float CalculateLeft()
    {
        return this.transform.position.x - (Mathf.Abs(this.transform.localScale.x) * (boxCollider.size.x / 2F)) + buffer;
    }

    private float CalculateBottom()
    {
        return this.transform.position.y - (Mathf.Abs(this.transform.localScale.y) * (boxCollider.size.y / 2F)) + buffer;
    }

    private float CalculateTop()
    {
        return this.transform.position.y + (Mathf.Abs(this.transform.localScale.y) * (boxCollider.size.y / 2F)) - buffer;
    }

    private void Debugging()
    {
        Debug.DrawLine(new Vector2(playerLeft, playerFeet), new Vector2(playerLeft, playerFeet - groundThreshold), Color.red);
        Debug.DrawLine(new Vector2(rigidBody.position.x, playerFeet), new Vector2(rigidBody.position.x, playerFeet - groundThreshold), Color.red);
        Debug.DrawLine(new Vector2(playerRight, playerFeet), new Vector2(playerRight, playerFeet - groundThreshold), Color.red);
        //Debug.DrawLine(new Vector2(playerRight, playerHead), new Vector2(playerRight + wallThreshold, playerHead), Color.red);
        Debug.DrawLine(new Vector2(playerRight, rigidBody.position.y), new Vector2(playerRight + wallThreshold, rigidBody.position.y), Color.red);
        //Debug.DrawLine(new Vector2(playerRight, playerFeet), new Vector2(playerRight + wallThreshold, playerFeet), Color.red);
        //Debug.DrawLine(new Vector2(playerLeft, playerHead), new Vector2(playerLeft - wallThreshold, playerHead), Color.red);
        Debug.DrawLine(new Vector2(playerLeft, rigidBody.position.y), new Vector2(playerLeft - wallThreshold, rigidBody.position.y), Color.red);
        //Debug.DrawLine(new Vector2(playerLeft, playerFeet), new Vector2(playerLeft - wallThreshold, playerFeet), Color.red);
    }
}
