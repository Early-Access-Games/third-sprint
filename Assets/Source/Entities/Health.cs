﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TeamIdentity))]
public class Health : MonoBehaviour
{
    public delegate void DeathHandler();
    public delegate void AliveHandler();
    private enum LifeState {
        Alive,
        Dead
    }

    public DeathHandler OnDeath;
    public AliveHandler OnAlive;

    public int HealthValue { get { return health; } }
    private int health = 100;
    private LifeState lastLifeState = LifeState.Alive;

    private void Update()
    {
        if (health <= 0)
            DeadState();
        else
            AliveState();
    }

    private void DeadState()
    {
        if (lastLifeState == LifeState.Alive)
        {
            lastLifeState = LifeState.Dead;
            if (OnDeath != null)
                OnDeath();
        }
    }

    private void AliveState()
    {
        if (lastLifeState == LifeState.Dead)
        {
            lastLifeState = LifeState.Alive;
            if (OnAlive != null)
                OnAlive();
        }
    }

    public void TakeDamage(int amount)
    {
        health -= amount;
    }
}
