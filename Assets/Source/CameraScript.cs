using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	// Target for camera to follow
	public GameObject target;
	private GameObject thePlayer;
	private Vector3 targetPos;

	private GameObject cameraAnchor;
	public static CameraScript current;

	// Scene boundaries
	[SerializeField]
	private float xMin, xMax, yMin, yMax;

	// Resize values
	[SerializeField]
	private float resizeTime;
	private float defaultCameraSize = 6.0f;

	void Awake(){
		current = this;
	}

	void Start () {
		thePlayer = GameObject.Find("Player");
		target = thePlayer;
	}
		
	void LateUpdate () {
		// Camera movement
		Camera.main.orthographic = true;
		targetPos = new Vector3 (Mathf.Clamp (target.transform.position.x, xMin, xMax), Mathf.Clamp (target.transform.position.y, yMin, yMax), transform.position.z);
		transform.position = Vector3.Lerp (transform.position, targetPos, 0.08f);
		if (Vector3.Distance (transform.position, target.transform.position) < 0.1f) {
			transform.position = targetPos;
		}
	
	  // Camera resizing-------------
		if (target == thePlayer) {
			// continually reduce size over time
			if (Camera.main.orthographicSize > defaultCameraSize) {
				Camera.main.orthographicSize -= (resizeTime * Time.deltaTime);
			}
		}

		if (target.tag == "Camera Anchor") {
			// get script of current anchor
			CameraAnchorScript anchorScript = target.GetComponent<CameraAnchorScript> ();
			// continually increase size over time
			if (Camera.main.orthographicSize < anchorScript.areaSize) {
				Camera.main.orthographicSize += (resizeTime * Time.deltaTime);
			}
		}
	  //-----------------------------
	}
}
