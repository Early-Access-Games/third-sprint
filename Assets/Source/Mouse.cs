﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour {
    private Transform player;
    private Vector3 relativeCenter;
    public float radiusFromPlayer = 15F;

    private void Awake() {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Start() {
        relativeCenter = player.position;
        this.transform.position = new Vector3(relativeCenter.x, relativeCenter.y, 10F);
    }

    private void Update() {
        this.transform.position += new Vector3(Input.GetAxis("MouseHorizontal_P1") / 10000F, Input.GetAxis("MouseVertical_P1") / 10000F);
        float distance = Vector3.Distance(this.transform.position, relativeCenter);
        if (distance > radiusFromPlayer) {
            Vector3 relativeDistance = this.transform.position - relativeCenter;
            relativeDistance *= radiusFromPlayer / distance;
            this.transform.position = relativeCenter + relativeDistance;
        }
        //for box constraints
        //this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, relativeCenter.x - radiusFromPlayer, relativeCenter.x + radiusFromPlayer),
        //                                        Mathf.Clamp(this.transform.position.y, relativeCenter.y - radiusFromPlayer, relativeCenter.y + radiusFromPlayer));
    }

    private void LateUpdate() {
        relativeCenter = player.position;
    }

    public Vector2 GetMousePosition() {
        return this.transform.position;
    }
}
