﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;
    private bool pause = false;
    public GameObject spawn;
    public List<GameObject> respawns = new List<GameObject>();
    private GameObject player;

    private void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        InitGame();
    }

    private void Start() {
        if (spawn == null) {
            Debug.LogError("Error: no initial spawn point set");
            Debug.Break();
        }
        else {
            respawns.Add(spawn);
            player.transform.position = spawn.transform.position;
        }
    }

    private void Update() {
        if (pause) {
            Time.timeScale = 0;
        }
        else {
            Time.timeScale = 1;
        }
    }

    private void InitGame() {
        //Load level, stuff from menu, spawning, set up references, etc
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void EndGame() {
        //unload memory and close game
    }

    public void TogglePause() {
        pause = !pause;
    }

    public void Respawn(GameObject player, GameObject respawn = null) {
        if (respawn != null) {
            player.transform.position = respawns[respawns.Count - 1].transform.position;
        }
        else {
            player.transform.position = respawn.transform.position;
        }
    }

    //checkpoint objects use these to add/remove themselves from list
    public void PushRespawn(GameObject respawn) {
        if (CheckDupe(respawn) == false){
                respawns.Add(respawn);
        }
    }

    public GameObject PopRespawn() {
        if (respawns.Count == 0) {
            Debug.LogError("Error: trying to pop empty respawn list");
            Debug.Break();
        }
        int last = respawns.Count - 1;
        GameObject tail = respawns[last];
        respawns.RemoveAt(last);
        return tail;
    }

    //returns false if not identical
    private bool CheckDupe(GameObject respawn) {
        foreach (GameObject item in respawns) {
            if (Object.ReferenceEquals(respawn, item))
                return true;
        }
        return false;
    }
}