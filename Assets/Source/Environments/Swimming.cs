﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//a terrible way to do it but also the simplest without causing more unexpected problems
//refactor all the code if we continue this on from this semester
public class Swimming : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.gameObject.GetComponent<PlayerController>().inWater = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.gameObject.GetComponent<PlayerController>().inWater = false;
        }
    }
}
