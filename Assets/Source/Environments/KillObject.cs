﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillObject : MonoBehaviour
{
    public GameObject obj; //the GameObject to destroy.

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Entity has triggered box.");
        //Two options: Move object or destroy it.
        //other.transform.position = new Vector3(-1, 1);
        Destroy(other.gameObject);
    }
}