using UnityEngine;

public enum Team {
Unset,
Player,
Enemy
}

public class TeamIdentity : MonoBehaviour
{
    public Team Id = Team.Unset;
}